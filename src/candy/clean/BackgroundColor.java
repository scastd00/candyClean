package candy.clean;

/**
 * Class that contains the colors' name that can be used in the game.
 *
 * @author Samuel Castrillo Domínguez
 * @version 1.1.0
 */
public enum BackgroundColor {

	BLACK, RED, GREEN, YELLOW, BLUE, PURPLE, CYAN, WHITE

}
